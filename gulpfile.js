var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

	/* 
     * All your styles
     */
    mix.styles([
        "../js/bower_components/bootstrap/dist/css/bootstrap.min.css",
        "../js/bower_components/font-awesome/css/font-awesome.min.css", // Font Awesome
        "../js/bower_components/startbootstrap-sb-admin-2/dist/css/sb-admin-2.css", // SB ADMIN 2 styles
        "main.css" // contains your general styles
    ]);


    mix.copy('resources/assets/js/bower_components/font-awesome/fonts', 'public/fonts');

    /* 
     * General JS Libraries Needed for each page 
     */
    mix.scripts([
        "/bower_components/jQuery/dist/jquery.min.js",
        "/bower_components/bootstrap/dist/js/bootstrap.min.js",
        "/bower_components/angular/angular.min.js",
        "/bower_components/angular-resource/angular-resource.min.js",
        "/bower_components/angular-bootstrap/ui-bootstrap.min.js",
        "/bower_components/moment/moment.js",
        "/bower_components/angular-filter/dist/angular-filter.min.js",
        "/bower_components/startbootstrap-sb-admin-2/dist/js/sb-admin-2.js", // SB Admin 2 script
        "/bower_components/metisMenu/dist/metisMenu.min.js", // SB Admin 2 MetisMenu
        "main.js" // contains your general scripts
    ],'public/js/general.js');


    /*
     * All your angular scripts
     */
    mix.scripts([

    	/* Your Main Angular File */
        "/angular/main.js",

        /* Your Controllers */
        // "/angular/controllers/[Your Controller].js",

        /* Your Services */
        // "/angular/services/[your service].js",

        /* Your Factories */
        // "/angular/services/[your factory].js",

    ],'public/js/scripts.js');
});
