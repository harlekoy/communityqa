<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/*
 * Login, register and logout alias routes
 */
Route::get('login', ['as'=>'/auth/login', 'uses'=>'Auth\AuthController@getLogin']);
Route::get('logout', ['as'=>'/auth/logout','uses'=>'Auth\AuthController@getLogout']);

Route::get('register', ['as'=>'/auth/register','uses'=>'Auth\AuthController@getRegister']);
Route::post('register', ['as'=>'/auth/register','uses'=>'Auth\AuthController@postRegister']);

Route::get('/dashboard', ['middleware' => 'auth', function(){
   return view('dashboard.index');
}]);